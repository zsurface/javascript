// KEY: eletron app, read file, show file

a = require("/Users/cat/myfile/bitbucket/jslib/aronjslib.js")

// Modules to control application life and create native browser window
const {app, BrowserWindow} = require('electron')
const {exec} = require('child_process')
const path = require('path')
const redis = require("redis");
const client = redis.createClient();


client.on("error", function(error) {
  console.error(error);
});
 
client.set("keykk", "Robberto Baggio\nPythagorean", redis.print);
client.get("keykk", redis.print);

exec('RedisReadKey', (err, stdout, stdin) => {
        if(err){
            return; 
        }
        client.get("keytable", redis.print);
        console.log(`stdout = ${stdout}`);
        console.log(`stdin = ${stdin}`);
    })

let fs = require('fs') 

function createWindow () {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }
  })

  mainWindow.setAlwaysOnTop(true, "floating"); 
  let fileName = "/Users/cat/myfile/bitbucket/testfile/popupshare.html"
  var prevWord = ''
  var currWord = 'kk'

  setInterval(function() {
          // 
          let data = fs.readFileSync(fileName, 'utf-8');
          // and load the index.html of the app.
          // mainWindow.loadFile('index.html')
          console.log('before data=[' + data + '] len=' + data.length)
          var cmd = data.replace(/\r\n|\r|\n/g, '')
          let ls = cmd.split(/\s+/) . filter(x => x.length > 0)
          console.log('ls=' + ls)
          if (ls.length > 1){
              let query = ls.slice(1).join(' ')
              let prefix = ls[0]
              let cmd = prefix + '+' + query 
              console.log('query=[' + query + ']')
              console.log('cmd=[' + cmd + ']')
              console.log('prevWord=[' + prevWord + '] currWord=[' + currWord + ']')
              if(prevWord != query){

                  const { net } = require('electron')
                  const request = net.request('http://localhost:8080/snippet?id=' + cmd)
                  request.on('response', (response) => {
                    console.log(`STATUS: ${response.statusCode}`)
                    console.log(`HEADERS: ${JSON.stringify(response.headers)}`)
                    response.on('data', (chunk) => {
                      var myls = chunk.toString().split('\n')
                      var strnl = chunk.toString()

                      client.set("prefix", prefix, redis.print);
                      client.set("keykk", strnl, redis.print);

                      exec('RedisReadKey', (err, stdout, stdin) => {
                                if(err){
                                    return; 
                                }
                                // client.get("keytable", redis.print);
                                client.get('keytable', function(err, result){
                                    if(err){
                                        console.log('error' + err)
                                        return
                                    }
                                    console.log('result=>' + result);

                                    var html = [
                                        "<body>",
                                          "<h1>It works</h1>",
                                        "</body>",
                                    ].join("");

                                    mainWindow.loadURL('data:text/html;charset=utf-8,' + encodeURI(result))
                                    // mainWindow.loadURL("data:text/html;charset=utf-8," + encodeURI(html));
                                });
                                console.log(`stdout = ${stdout}`);
                                console.log(`stdin = ${stdin}`);
                          })

                      console.log('myls=[' + myls + ']')
                      console.log(`BODY: ${chunk}`)
                    })
                    response.on('end', () => {
                      console.log('No more data in response.')
                    })
                  })
                  request.end()

                // mainWindow.loadURL('http://localhost:8080/snippet?id=' + cmd)
                prevWord = currWord
                currWord = query 
              }

          }
  }, 1000)

  // Open the DevTools.   list 
  // mainWindow.webContents.openDevTools()
}

//      

app.on('ready', () => {
  const { net } = require('electron')
  const request = net.request('https://github.com')
  request.on('response', (response) => {
    console.log(`STATUS: ${response.statusCode}`)
    console.log(`HEADERS: ${JSON.stringify(response.headers)}`)
    response.on('data', (chunk) => {
      console.log(`BODY: ${chunk}`)
    })
    response.on('end', () => {
      console.log('No more data in response.')
    })
  })
  request.end()
})


// 
// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
