const lib = require('./aronlib.js');

console.log('dog');


var list = ['dog', 'cat'];
lib.prefixMap(list);


console.log(lib.take(1, [1, 2, 3]));
console.log(lib.drop(1, [1, 2, 3]));
console.log(lib.compareArray(lib.prefixStr('abc'), ['a', 'ab', 'abc']));

var m = lib.prefixMap(['a', '12']);
var expected = new Map(); 
expected.set('a', ['ab']);
expected.set('ab', ['ab']);
console.log(m);
// console.log(expected);

function chop(n, str){
    var fst = take(n, str);
    var snd = drop(n, str);
    return {
        fst : fst,
        snd : snd
    };
}

function take(n, str){
    return str.substr(0, n);
}
function drop(n, str){
    return str.substr(n, str.length);
}


function prefixMap2(list){
    var map = new Map();
    for(var i=0; i<list.length; i++){
        for(var j=0; j<list[i].length; j++){
            var ps = chop(j + 1, list[i]);
            var v = map.get(ps.fst);
            if(v == undefined){
                v = [];
            }
            v.push(ps.snd);
            map.set(ps.fst, v);
        }
    }
    return map;
}
var map = prefixMap2(['dog', 'cat']);
console.log(map);
console.log(chop(1, 'dog'));
console.log(chop(2, 'dog'));

